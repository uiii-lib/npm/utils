import { takeOne } from "./take.js";

export function ensure<T>(it: T): NonNullable<T>;
export function ensure<T, R extends T>(it: T, guard: (it: T) => it is R): R;
export function ensure<T, R extends T>(it: T, guard?: (it: T) => it is R): R {
	const nonNullableGuard = (it: T): it is NonNullable<T> =>
		it !== undefined && it !== null;

	if (!guard && !nonNullableGuard(it)) {
		throw new Error("Cannot ensure non-nullable value");
	}

	if (guard && !guard(it)) {
		console.dir(it, { depth: 10 });
		throw new Error("Cannot ensure");
	}

	return it as R;
}

export function ensured<T, R extends T>(guard: (it: T) => it is R) {
	return (it: T): it is R => Boolean(ensure(it, guard));
}

export async function ensureOne<T>(it: AsyncIterator<T, void>) {
	const value = await takeOne(it);

	if (!value) {
		throw new Error("Cannot ensure one");
	}

	return value;
}
