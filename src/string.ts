function words(value: string) {
	return value
		.replace(/[-_\.]+/g, " ")
		.replace(/([a-z\d])([A-Z])/g, "$1 $2")
		.toLowerCase();
}

export function kebabCase(value: string) {
	return words(value).replace(/\s+/g, "-");
}

export function camelCase(value: string) {
	return words(value).replace(/\s+(.)/g, (_, letter) => letter.toUpperCase());
}

export function pascalCase(value: string) {
	return words(value).replace(/(^|\s+)(.)/g, (_, __, letter) =>
		letter.toUpperCase(),
	);
}
