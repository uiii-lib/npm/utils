export type DeepSelector<T> = T extends (infer U)[]
	? U extends object
		? DeepSelector<U>
		: true
	: T extends object
		? {
				[P in keyof T]?: DeepSelector<T[P]>;
			}
		: true;

export type DeepSelected<T, S> = S extends true
	? T
	: {
			[P in keyof S]: P extends keyof T ? DeepSelected<T[P], S[P]> : never;
		};

export function deepSelect<T, S extends DeepSelector<T>>(
	data: T | undefined,
	propsSelector: S | undefined,
): DeepSelected<T, S> {
	if (data === undefined) {
		return undefined as any;
	}

	if (propsSelector === undefined) {
		return undefined as any;
	}

	if (propsSelector === true) {
		return data as any;
	}

	const result: any = {};

	for (const prop in propsSelector) {
		result[prop] = deepSelect((data as any)[prop], propsSelector[prop] as any);
	}

	return result;
}
