export async function take<T>(count: number, it: AsyncIterableIterator<T>) {
	const values: T[] = [];

	for (let i = 0; i < count; ++i) {
		const result = await it.next();

		if (result.done) {
			break;
		}

		values.push(result.value);
	}

	return values;
}

export async function takeOne<T>(it: AsyncIterator<T, void>) {
	return (await it.next()).value || undefined;
}
